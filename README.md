# Nvidia Jetson NANO에 Springgo Router 설치하기
---
## 설치하기전 사전 작업
* 개발 환경
```bash
cat /etc/nv_tegra_release
# R32 (release), REVISION: 4.2, GCID: 20074772, BOARD: t210ref, EABI: aarch64, DATE: Thu Apr  9 01:22:12 UTC 2020
```
* H/W 사양
```text
Mem:            3.9G
Swap:           1.9G
Architecture:   aarch64
CPU(s):         4
Vendor ID:      ARM
Model name:     Cortex-A57
CPU max MHz:    1479.0000
L1d cache:      32K
L1i cache:      48K
L2 cache:       2048K
```
* S/W 구성
![Alt text](./images/springgo_router_sw_v20.png "Springgo-Router S/W 2.0 구조도")

* SSH root로 접근하기
```bash
sudo passwd root  # 암호는 사전 협의해야함.
vi /etc/ssh/sshd_config
PermitRootLogin yes
```
* docker-compose 설치하기
```bash
sh booting.sh
docker-compose --version  # 확인바람.
```

## Springgo Router 설치하기
* 설치
```bash
# pwd /root
git clone https://byungchul@bitbucket.org/springgos/sgr_prod.git -b master
cd sgr_prod
pm2 start pm2.json
pm2 logs  # 진행 확인바람.
```
* 자동실행
```bash
pm2 startup
reboot  # 부팅후 확인바람.
```

## Springgo Router S/W 진단 및 점검
* 진단 및 점검
```bash
cd /root/sgr_prod
pm2 monit  # pm2로 확인
docker-compose ps  # container 확인
docker-compose logs -f --tail=100 <service_name>  # docker logs로 확인
docker-compose exec <service_name> pm2 list  # container pm2로 확인
docker-compose exec <service_name> pm2 log <process_id>  # container pm2로 확인
```
* PM2를 종료하고 직접 Docker Compose로 진단 하는 방법
```bash
cd /root/sgr_prod
pm2 stop all
docker-compose up -d master apps drivers kafka
docker-compose logs -f --tail=100 <service_name> or <empty string>
docker-compose exec <service_name> pm2 log <process_id>  # container pm2로 확인
docker-compose stop|start|down  # container control
docker-compose exec <service_name> pm2 restart <process_id>  # container pm2로 process 재시작
```
* Device 확인
```bash
pm2 list; pm2 logs  # booting후 확인
ls /dev/ttyACM*  # can device 확인
ls /dev/video*   # video 확인
ls /dev/ttyTHS*  # gnss 확인
ls /dev/ttyUSB*  # dtg 확인
ifconfig can*    # socket can 확인
```
* Device 확인후 이상시 Router 전원 On/Off
## ROS Topic 확인
* Kafak Producer topic 확인
```bash
docker-compose exec kafka bash -c "source /ros_entrypoint.sh; rostopic list | grep r2k*"  # topic 확인
docker-compose exec kafka bash -c "source /ros_entrypoint.sh; rostopic echo /r2k_<device_name>"  # echo
```
* Device topic 확인
```bash
docker-compose exec drivers bash -c "source devel/setup.bash; rostopic list"  # topic 확인
docker-compose exec drivers bash -c "source devel/setup.bash; rostopic echo <device_name>"  # echo
```
* 모든 Topic 진단
```bash
docker-compose exec drivers bash -c "source devel/setup.bash; rostopic echo /diagnostics"
```
